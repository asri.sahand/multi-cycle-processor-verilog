module acc(input[15:0] write_data, input clk, write_en ,output[15:0] out,output zero);
  reg[15:0] memory;
  assign out=memory;
  assign zero=(memory==0)?1:0;
  always@(posedge clk) begin
    if(write_en)
      memory=write_data;
  end
endmodule