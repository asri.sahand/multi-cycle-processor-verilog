module mem(input[15:0] adr, input[15:0] writeData, input clk, write, output[15:0] out);
  reg[15:0] memory[0:2**15];
  initial $readmemb("data.txt", memory);
  assign out = memory[adr];
  always@(posedge clk) begin
	 if(write) begin
	 	 memory[adr] = writeData;
	 end
  end
  //always @(posedge clk) $display("memory[200] : %b", memory[200]);
endmodule
