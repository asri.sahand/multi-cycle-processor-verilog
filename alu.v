module alu(input[1:0] alu_op, input[15:0] in0, in1,output[15:0] alu_out);
  wire[15:0] Add, Sub, And, Not;
  
  assign Add = in0+in1;
  assign Sub = in0-in1;
  assign And = in0&in1;
  assign Not = ~in0;
  
  assign alu_out = (alu_op==2'b00)? Add : (alu_op==2'b01)? Sub :
   (alu_op==2'b10)? And : (alu_op==2'b11)? Not : alu_out;
  
endmodule
