module reg15(input[15:0] in, input clk, output reg[15:0] out);
  always@(posedge clk)
    out <= in;
endmodule