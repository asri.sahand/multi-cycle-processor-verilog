module signExt(input[12:0] in,output[15:0] out);
  assign out=in[12]?{3'b111,in[12:0]}:{3'b000,in[12:0]};
endmodule