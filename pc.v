module pc(input[15:0] in, input rst, input clk, input pc_en, output reg[15:0] out);
	always@(posedge clk) begin
	 if(pc_en) begin
	   if(rst)
	     out = 0;
	   else
	     out = in;
	 end
	end
endmodule