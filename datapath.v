module datapath(input clk, rst,input pc_src, pc_en, mem_src, mem_write_en,
 IR_en, acc_write_en, alu_src0, alu_src1, acc_src,input[1:0] alu_op, output zero,output[15:0] ins);
 
  wire[15:0] pc_in, pc_out;
  wire[15:0] mem_out, IR_out, MDR_out, sign_extend_out;
  wire[15:0] acc_writedata,mem_adr;
  wire[15:0] alu_in1,alu_in0,acc_out,alu_out;
  wire[15:0] One = 16'b0000000000000001;
  wire pcWr;
  wire[15:0] aluout_out;
  mux1 PCMUX(alu_out, sign_extend_out, pc_src, pc_in);
  assign pcWr=(pc_en==1'b1)?1'b1:(zero==1'b1 && ins[15:13]==3'b111)?1'b1:1'b0;
  pc PC(pc_in, rst, clk, pcWr, pc_out);
  mux1 MEMMUX(pc_out, sign_extend_out, mem_src, mem_adr);
  mem MEM(mem_adr, acc_out, clk, mem_write_en, mem_out);
  IR ir(mem_out, clk, IR_en, IR_out);
  reg15 MDR(mem_out, clk, MDR_out);
  signExt SIGNEX(IR_out[12:0], sign_extend_out);
  mux1 ACCMUX(aluout_out, MDR_out, acc_src, acc_writedata);
  acc ACC(acc_writedata, clk, acc_write_en , acc_out,zero);
  mux1 ALUMUX0(pc_out, acc_out, alu_src0, alu_in0);
  mux1 ALUMUX1(One, MDR_out, alu_src1, alu_in1);
  alu ALU(alu_op, alu_in0, alu_in1, alu_out);
  reg15 ALUOUT(alu_out, clk, aluout_out);
  assign ins=IR_out;
  
endmodule
