module IR(input[15:0] in, input clk, input IR_en, output reg[15:0] out);
  always@(posedge clk)
    if(IR_en)
      out <= in;
endmodule
