module all();
  reg clk,rst,start;
  wire pc_src,pc_en,mem_src,mem_write,IR_en,acc_write_en,alu_src0,alu_src1,acc_src,zero;
  wire[1:0] alu_op;
  wire[15:0] ins;
  datapath dp(clk, rst,pc_src, pc_en, mem_src, mem_write,IR_en, acc_write_en, alu_src0, alu_src1,acc_src, alu_op,zero,ins);
  controller c(start ,clk ,rst,ins,pc_src,pc_en,mem_src,mem_write,IR_en,acc_src,acc_write_en,alu_src0,alu_src1,alu_op);
  initial repeat(100) #49 clk = ~clk;
  initial begin
	start=1;
    clk = 0;
    rst = 1;
    #250 rst = 0;
  end
  

endmodule
