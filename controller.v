module controller(input start ,clk ,reset, input [15:0] ins,output reg pcSrc,pcWrite,memSrc,memWrite,ldIR,accSrc,accWrite,ALU0,ALU1, output reg [1:0]ALUop);
	localparam [4:0] IF = 0 , ID = 1 , ADD = 2 , SUB = 3 , AND = 4 , NOT = 5 , LDA = 6 , STA = 7 , JUMP = 8 , JZ = 9,
		ALUSELADD=10,ALUSELSUB=11,ALUSELAND=12,ACCSELNOT=13,ACCSELLDA=14,ACCSRCADD=15,ACCSRCSUB=16,ACCSRCAND=17;
	reg [4:0] ps = IF , ns ;
 	always @(posedge reset , posedge clk) begin
		if (reset) 
			ps = IF;
		else 
			ps = ns;
	end
	always@(ps , ins[15:13], start)begin
		case (ps)
			IF : ns = start ? ID : IF ;
			ID : begin
				case(ins[15:13])
					3'b000: ns=ADD;
					3'b001: ns=SUB;
					3'b010: ns=AND;
					3'b011: ns=NOT;
					3'b100: ns=LDA;
					3'b101: ns=STA;
					3'b110: ns=JUMP;
					3'b111: ns=JZ;
				endcase
			end
			ADD: ns=ALUSELADD;
			SUB: ns=ALUSELSUB;
			AND: ns=ALUSELAND;
			NOT: ns=ACCSELNOT;
			LDA: ns=ACCSELLDA;
			STA: ns=IF;
			JUMP: ns=IF;
			JZ: ns=IF;
			ALUSELADD: ns=ACCSRCADD;
			ALUSELSUB: ns=ACCSRCSUB;
			ALUSELAND: ns=ACCSRCAND;
			ACCSELNOT: ns=IF;
			ACCSELLDA: ns=IF;
			ACCSRCADD: ns=IF;
			ACCSRCSUB: ns=IF;
			ACCSRCAND: ns=IF;
		endcase
	end
	
	always@ (ps) begin
		pcSrc=0;pcWrite=0;memSrc=0;memWrite=0;ldIR=0;accSrc=0;accWrite=0;ALU0=0;ALU1=0;ALUop=2'b00;
		case(ps)
			IF : begin pcWrite=1;ldIR=1; end
			//ID : begin pcWrite=1; end
			ADD: memSrc=1;
			SUB: memSrc=1;
			AND: memSrc=1;
			NOT: begin ALUop=2'b11;ALU0=1;end
			LDA: memSrc=1;
			STA: begin memSrc=1;memWrite=1;end
			JUMP:begin pcSrc=1;pcWrite=1;end
			JZ: pcSrc=1;
			ALUSELADD: begin ALU0=1;ALU1=1;ALUop=2'b00;end
			ALUSELSUB: begin ALU0=1;ALU1=1;ALUop=2'b01;end
			ALUSELAND: begin ALU0=1;ALU1=1;ALUop=2'b10;end
			ACCSELNOT: accWrite=1;
			ACCSELLDA: begin accSrc=1;accWrite=1; end
			ACCSRCADD: accWrite=1;
			ACCSRCSUB: accWrite=1;
			ACCSRCAND: accWrite=1;
		endcase
	end
endmodule
